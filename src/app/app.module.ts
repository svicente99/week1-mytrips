import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaViajesComponent } from './lista-viajes/lista-viajes.component';
import { AlbumViajesComponent } from './album-viajes/album-viajes.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaViajesComponent,
    AlbumViajesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
