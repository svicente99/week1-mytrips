import { Component, Input, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';


@Component({
  selector: 'app-lista-viajes',
  templateUrl: './lista-viajes.component.html',
  styleUrls: ['./lista-viajes.component.less']
})
export class ListaViajesComponent implements OnInit {

  @Input() viajeForm: viaje_form;
  
  username: string = "";
  nu_viaje : number = 0;
  
  arrViajes: number[] = Array();

  listViajes = [
    { id: 1, nombre: "Recife" },
    { id: 2, nombre: "Olinda" },
    { id: 3, nombre: "Recife-Sirinhaém" },
    { id: 4, nombre: "Sirinhaém-Porto das Pedras" },
    { id: 5, nombre: "Ilha da Crôa (Barra Santo Antônio)" },
    { id: 6, nombre: "Aracaju-Caueira" },
    { id: 7, nombre: "Praia do Forte-Salvador" },
    { id: 8, nombre: "Salvador" }
  ];
  
  constructor() { 
  	  
	  this.viajeForm = {
		  txtNombre: "escribe tu nombre...",
		  selViaje : "0"
	  };
  }

  ngOnInit(): void {
  }
  
  salvar(nombre: string, nu_viaje: string) : boolean {
	  this.username = (nombre=="" ? "Señor(a)" : nombre);
	  this.nu_viaje  = parseInt(nu_viaje);
	  // alert(this.username+'\n'+this.nu_viaje);
	  this.arrViajes.push( this.nu_viaje );
	  // to debug inserting in array of Destino-Viajes
	  // for(var v in this.arrViajes) alert(this.arrViajes[v]);

	  return false;
  }

}

export class viaje_form {
  txtNombre:string;
  selViaje :string;
  
  constructor() {
	this.txtNombre = '';
	this.selViaje  = '';
  }
} 
