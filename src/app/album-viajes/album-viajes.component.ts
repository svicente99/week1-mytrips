import { Component, Input, HostBinding, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';


@Component({
  selector: 'app-album-viajes',
  templateUrl: './album-viajes.component.html',
  styleUrls: ['./album-viajes.component.less']
})

export class AlbumViajesComponent implements OnInit {

  @Input() nombre_visitante: string = "";
  @Input() viaje_selected:   number = 0;
  @HostBinding('attr.class') cssClass = "col-md-4";
  
  album: DestinoViaje[] = Array();
  album_titulo: string  = "Nombre da viagem";
  descripcion:  string  = "Descripción";
  fecha_viaje:  string  = "";
  thumbnail:    string  = "";
  album_url:    string  = "";
  
  constructor() { 
	this.album.push( new DestinoViaje(
		1, "Recife",
		//"Visita aos principais pontos turísticos da cidade, alugando uma bicicleta pública. Destaque para o Instituto Ricardo Brennant",
		"Visita a los principales puntos turísticos de la ciudad, renta de bicicleta pública. Destacado para el Instituto Ricardo Brennant.", 
		"16-11-2019", 
		"https://photos.app.goo.gl/AVh2sqfB5n3AMiXi6",
		"Trip1_IMG_20191116_173040.jpg") );
	this.album.push( new DestinoViaje(
		2, "Olinda",
		//"Um passeio pelo Centro Histórico da primeira capital de Pernambuco; suas igrejas e a cultura local",
		"Un paseo por el Centro Histórico de la primera capital de Pernambuco; sus iglesias y cultura local ",
		"15-11-2019", 
		"https://photos.app.goo.gl/GVegXHbbMshJcBAN9",
		"Trip2_IMG_20191115_163112.jpg") );
	this.album.push( new DestinoViaje(
		3, "Recife-Sirinhaém",
		//"Começo da viagem de bicicleta (cicloviagem). Passando por Jaboatão dos Guararapes e Porto de Galinhas",
		"Comienzo del viaje en bicicleta (**biciviaje**). Pasando por Jaboatão dos Guararapes y Porto de Galinhas.",
		"17-11-2019", 
		"https://photos.app.goo.gl/UAWS1hkAjEUQ36DZ8",
		"Trip3_IMG_20191117_104339.jpg") );	
 	this.album.push( new DestinoViaje(
		4, "Porto de Pedras-Ilha da Croa",
		//"Trecho muito bonito do percurso, no estado das Alagoas, e também o mais duro fisicamente. Destaque para São Miguel dos Milagres e sua natureza exuberante!",
		"Muy bonito tramo de la ruta, en el estado de Alagoas, y también el más duro físicamente. ¡Destaque para São Miguel dos Milagres y su naturaleza exuberante!",
		"19-11-2019", 
		"https://photos.app.goo.gl/UAWS1hkAjEUQ36DZ8",
		"Trip4_DSCN3164.JPG") );		
 	this.album.push( new DestinoViaje(
		5, "Ilha da Crôa (Barra de Santo Antônio)",
		//"Ponto culminante de toda a viagem, a Ilha da Crôa é um local paradisíaco. Preservada, uma bela praia, nativos hospitaleiros. Foi meu refúgio e descanso.",
		"Punto culminante de todo el viaje, Ilha da Crôa es un lugar paradisíaco. Conservada, una hermosa playa, nativos hospitalarios. Fue mi refugio y descanso.",
		"20 a 21-11-2019", 
		"https://photos.app.goo.gl/UAWS1hkAjEUQ36DZ8",
		"Trip5_IMG_20191120_170608.jpg") );		
 	this.album.push( new DestinoViaje(
		6, "Aracaju",
		//"Conheci a capital do Estado de Sergipe. Visitei o principal museu, comi um ótimo almoço vegetariano e observei um pôr-do-sol maravilhoso!",
		"Conocí la capital del estado de Sergipe. Visité el museo principal, comí un excelente almuerzo vegetariano y vi una maravillosa puesta de sol.",
		"22-11-2019", 
		"https://photos.app.goo.gl/TJbEWLSyBMSnk2vP9",
		"Trip6_IMG_20191122_173040.jpg") );		
 	this.album.push( new DestinoViaje(
		7, "Praia do Forte-Salvador",
		//"Último trecho pedalado, aproximando-se da capital da Bahia, completando os quase 650 km; uma cicloviagem inesquecível.",
		"Último tramo pedaleado, acercándose a la capital bahiana, completando casi 650 km; un recorrido en bici inolvidable.",
		"25-11-2019", 
		"https://photos.app.goo.gl/J1DwuGw12wZv8xzU7",
		"Trip7_DSCN3446.JPG") );		
 	this.album.push( new DestinoViaje(
		8, "Salvador",
		//"Um dos 2 dias da visita à parte histórica de Salvador. Suas igrejas, o conjunto arquitetônico da cidade Alta e Baixa, uma volta às riquezas culturais do passado.",
		"Uno de los 2 días de la visita al casco histórico de Salvador. Sus iglesias, el conjunto arquitectónico de la ciudad alta y baja, una vuelta a las riquezas culturales del pasado.",
		"26-11-2019", 
		"https://photos.app.goo.gl/J1DwuGw12wZv8xzU7",
		"Trip8_IMG_20191125_144643.jpg") );		
    
  }	
	
  ngOnInit(): void {
	  
	for(var index in this.album) {
		if(this.album[index].nuViaje==this.viaje_selected) {
		   this.album_titulo = this.album[index].title;
		   this.thumbnail = this.album[index].picture;
		   this.album_url = this.album[index].url_album;
		   this.descripcion = this.album[index].summary;
		   this.fecha_viaje = this.album[index].date;
		}
	}
	  
  }

}

