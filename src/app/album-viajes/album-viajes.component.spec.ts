import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumViajesComponent } from './album-viajes.component';

describe('AlbumViajesComponent', () => {
  let component: AlbumViajesComponent;
  let fixture: ComponentFixture<AlbumViajesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumViajesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumViajesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
