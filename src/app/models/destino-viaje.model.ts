export class DestinoViaje {
	
	nuViaje:	number;
	title: 		string;
	summary:	string;
	date:		string;
	url_album:	string;
	picture:	string;
	
	
	constructor(v:number, t:string, s:string, d:string, u:string, p:string) {
		this.nuViaje = v;
		this.title   = t;
		this.summary = s;
		this.date    = d;
		this.url_album = u;
		this.picture = p;
	}	
	
}
