# Mytrips

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.1.

## Publish

The project was published as a web page in [http://www.supersuporte.com/Coursera/Angular/Austral_university/project1](http://www.supersuporte.com/Coursera/Angular/Austral_university/project1)
It was fully tested in two browsers (Chrome and Firefox) and Android and iPhone mobile phones, as well.

Anyway, every trouble can be reported to my twitter.

## User story

It's an assignment to Week-1 of course "Desarrollo de p�ginas con Angular by Universidad Austral". 
It shows a list of my trips that I took during my last vacation (Northeast Brazil) and their respective photos saved in Google photos.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

